# dofiles

Configs etc.

run `setup.ps1`

This will install all tools and configure.

`install.ps1` just installs, `configure.ps1` just configures

What's left to do:
- Configure Jetbrains Scripts
    - Open the Toolbox and set "Generate Shell Scripts" to `C:\scripts\jetbrains`
- Configure Fonts
    - install Meslo LG M DZ Bold Nerd Font Complete Mono Windows Compatible
    - set as default in windows terminal
- Configure Windows Terminal
    - set as default terminal
    - set Powershell 7 as default profile
- Remove all the crap from startup
    - `msconfig`
    - `taskmgr`
- Pin favorites to Explorer 
    - Mounted Dev Drive (X:/)
    - Work folder from documents
    - C:/scripts
- Configure IntelliJ recent projects
    - put `recentProjects.xml` in `C:\Users\{username}\.IntelliJIdea{version}\config\options`
- Pin quick access "Folders" to start menu action row (at the bottom)
    - Personal Folder
    - Explorer
    - Settings

# TODOs to automate
- Link recentProjects.xml to current IntelliJ version somehow
