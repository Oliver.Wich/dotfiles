#Requires -RunAsAdministrator

$UserHome = $ENV:USERPROFILE

function createSymlink {
	param (
		[Parameter(Mandatory=$true, Position=0)]
        [string]$Target,
		[Parameter(Mandatory=$true, Position=1)]
        [string]$Source
    )

	$Target = $Target -replace "/","\"
	$Source = $Source -replace "/","\"
	$Source = Resolve-Path $Source

	echo "Creating symlink from $Source to $Target..."

	New-Item -itemtype SymbolicLink -path $Target -value $Source -Force
}

function createSymlinkForFile {
	param (
		[Parameter(Mandatory=$true, Position=0)]
        [string]$Path,
		[Parameter(Mandatory=$true, Position=1)]
        [string]$Filename,
		[string]$LocalPath
    )

	$Target = "$Path\$Filename"
	$Source = "$LocalPath$Filename"

	createSymlink $Target $Source
}

function configurePHP {
	$PHPPath = (gcm -Syntax php) -replace ".{8}$"
	if ($PHPPath){
		Write-Host "Downloading recent cacert.pem..." -ForegroundColor DarkYellow
		Invoke-WebRequest https://curl.se/ca/cacert.pem -OutFile $PHPPath\cacert.pem
	} else {
		Write-Host "Php is not installed! Please install and run again to configure.." -ForegroundColor Red
	}
}

function configurePowershellProfile {
	if (!!(Get-InstalledModule PSReadLine -MinimumVersion 2.2.0-beta4 -AllowPrerelease)) {
		Write-Host "PSReadLine allready installed with a version never or equal 2.2.0-beta4!" -ForegroundColor DarkRed
	} else {
		Write-Host "Updating PSReadLine..." -ForegroundColor DarkYellow
		pwsh -noprofile -command "Install-Module PSReadLine -Force"
	}

	Write-Host "Configuring Powershell Profile" -ForegroundColor DarkYellow
	if ([System.IO.File]::Exists($profile)) {
		echo "Old Profile exists. Removing..."
		rm $profile
	}

	if ([System.IO.File]::Exists("$UserHome/.oliverwich.omp.yml")) {
		echo "Old Theme exists. Removing..."
		rm "$UserHome\.oliverwich.omp.yml"
	}

	createSymlinkForFile $UserHome '.oliverwich.omp.yml'

	createSymlink $profile 'Microsoft.PowerShell_profile.ps1'
}




# Configure powershell profile
configurePowershellProfile


Write-Host "Configuring Git" -ForegroundColor DarkGreen
createSymlinkForFile $UserHome ".gitconfig"


Write-Host "Configuring WSL " -ForegroundColor DarkGreen
createSymlinkForFile $UserHome ".wslconfig"


Write-Host "Configuring SSH " -ForegroundColor DarkGreen
createSymlinkForFile "$UserHome/.ssh" "config"

Write-Host "Creating scripts dir " -ForegroundColor DarkGreen
createSymlink "C:\\scripts" "scripts"


Write-Host "Configuring PHP" -ForegroundColor DarkGreen
configurePHP

# TODO:Configure Core Temp. find path, symlink coretemp.ini


Write-Host "Configuring Path" -ForegroundColor DarkGreen
$oldpath = (Get-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH).path
if (!$oldpath.EndsWith(';')) {
	$oldpath = $oldpath + ";"
}
if ($oldpath -Match "C:\\scripts;") {
	Write-Host "C:\scripts is allready in PATH!" -ForegroundColor DarkRed
} else {
	$newpath = “$oldpath" + "C:\scripts;”
	Set-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH -Value $newPath
}

if ($oldpath -Match "C:\\scripts\\jetbrains;") {
	Write-Host "C:\scripts\jetbrains is allready in PATH!" -ForegroundColor DarkRed
} else {
	$newpath = “$oldpath" + "C:\scripts\jetbrains;”
	Set-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH -Value $newPath
}
